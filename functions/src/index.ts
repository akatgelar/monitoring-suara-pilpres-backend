const functions = require('firebase-functions');
const admin = require('firebase-admin');
const jwt = require('jwt-simple');
const md5 = require('md5');
const dateFormat = require('dateformat');

const serviceAccount = require("./secret-key.json");
const cors = require('cors')({
    origin: true
  });

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://monitoring-suara-pilpres-2019.firebaseio.com"
});
 
const db = admin.firestore();
db.settings({ timestampsInSnapshots: true });


function checkToken(request){

    if(!request.headers.authorization){
        return {status:false, message:"Don't have JWT token.", result:{}};
    } else {

        try { 
            let bearer = request.headers.authorization; 
            bearer = bearer.split(" "); 
            console.log(bearer[1]);
            const decoded = jwt.decode(bearer[1], 'secret', false, 'HS256'); 
            console.log(decoded);
            

            if(!decoded['exp'] || !decoded['iat'] || !decoded["_id"]){             
                return {status:false, message:"JWT token missmatch.", result:{}};

            } else {
                
                if(decoded["exp"] <= (new Date().getTime() / 1000)){
                    return {status:false, message:"Token expired.", result:{}};

                } else {
       
                    return db.collection("test_user")
                    .where("_id", "==", decoded["_id"])
                    .get()
                    .then(snapshot => { 
                        if(snapshot.size > 0){ 
                            return {status:true, message:"", result:{}};
                        } else {
                            return {status:false, message:"User don't have access", result:{}};
                        }
                    }).catch(reason => {  
                        return {status:false, message:"Get User failed", result:{}};
                    })                   
                }      
            }
        } catch (error) {
            console.log(error);
            return {status:false, message:"JWT token signature failed.", result:{}};
        } 
    }
}

function updateLastAccess(id, last_access){

    db.collection("test_user")
    .doc(id)
    .update({
        "last_access": last_access
    })
    .then(function() {
        console.log("Last access updated");
    })
    .catch(function(error) {
        console.log("Error update last access:", error);
    });

}
 
function rekapKelurahan(kode_kelurahan, status){
 
    let suara_1 = 0;
    let suara_2 = 0;
    let suara_total = 0;
    let tps_belum = 0;
    let tps_masuk = 0;
    let tps_total = 0;
    db.collection("test_tps")
    .where("kode_kelurahan", "==", kode_kelurahan )
    .get()
    .then(snapshot => {

        snapshot.forEach(doc => {
            suara_1 = suara_1 + Number(doc.data().suara_1);
            suara_2 = suara_2 + Number(doc.data().suara_2);
            tps_total = tps_total + 1;
            if(doc.data().suara_scan_1 !== ""){
                tps_masuk = tps_masuk + 1;
            }
        });
        
        suara_total = suara_1+suara_2;
        tps_belum = tps_total-tps_masuk;
        console.log("rekap kelurahan");
        console.log("suara_1 : "+suara_1);
        console.log("suara_2 : "+suara_2);
        console.log("suara_total : "+suara_total);
        console.log("tps_total : "+tps_total);
        console.log("tps_masuk : "+tps_masuk);
        console.log("tps_belum : "+tps_belum);

        if(status === true) {
            // update rekap kelurahan        
            db.collection("test_kelurahan")
            .doc(kode_kelurahan) 
            .update({
                "validasi.suara_1": suara_1,
                "validasi.suara_2": suara_2,
                "validasi.suara_total": suara_total,
                "validasi.tps_total": tps_total,
                "validasi.tps_masuk": tps_masuk,
                "validasi.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kelurahan sukses")

                let kode_wilayah = kode_kelurahan.split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1]+"."+kode_wilayah[2];
                setTimeout(function () {
                    rekapKecamatan(kode_wilayah, true); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kelurahan error :", error);
            });
        }else {
            
            // update rekap kelurahan        
            db.collection("test_kelurahan")
            .doc(kode_kelurahan) 
            .update({
                "validasi_non.suara_1": suara_1,
                "validasi_non.suara_2": suara_2,
                "validasi_non.suara_total": suara_total,
                "validasi_non.tps_total": tps_total,
                "validasi_non.tps_masuk": tps_masuk,
                "validasi_non.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kelurahan sukses")

                let kode_wilayah = kode_kelurahan.split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1]+"."+kode_wilayah[2];
                setTimeout(function () {
                    rekapKecamatan(kode_wilayah, false); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kelurahan error :", error);
            });
        }
    })
    .catch(function(error) {
        console.log("rekap kelurahan error:", error);
    });

}

function rekapKecamatan(kode_kecamatan, status){

    let suara_1 = 0;
    let suara_2 = 0;
    let suara_total = 0;
    let tps_belum = 0;
    let tps_masuk = 0;
    let tps_total = 0;
    db.collection("test_kelurahan")
    .where("kode_kecamatan", "==", kode_kecamatan )
    .get()
    .then(snapshot => {

        if(status === true){
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi.suara_2);
                tps_total = tps_total + Number(doc.data().validasi.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi.tps_masuk); 
            });
        }else{
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi_non.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi_non.suara_2);
                tps_total = tps_total + Number(doc.data().validasi_non.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi_non.tps_masuk); 
            });
        }
        
        suara_total = suara_1+suara_2;
        tps_belum = tps_total-tps_masuk;
        console.log("rekap kecamatan"); 
        console.log("suara_1 : "+suara_1);
        console.log("suara_2 : "+suara_2);
        console.log("suara_total : "+suara_total);
        console.log("tps_total : "+tps_total);
        console.log("tps_masuk : "+tps_masuk);
        console.log("tps_belum : "+tps_belum);

        if(status === true){

            // update rekap kelurahan        
            db.collection("test_kecamatan")
            .doc(kode_kecamatan) 
            .update({
                "validasi.suara_1": suara_1,
                "validasi.suara_2": suara_2,
                "validasi.suara_total": suara_total,
                "validasi.tps_total": tps_total,
                "validasi.tps_masuk": tps_masuk,
                "validasi.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kecamatan sukses")

                let kode_wilayah = kode_kecamatan.split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1];
                setTimeout(function () {
                    rekapKota(kode_wilayah, true); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kecamatan error :", error);
            });
        }else{
            
            // update rekap kelurahan        
            db.collection("test_kecamatan")
            .doc(kode_kecamatan) 
            .update({
                "validasi_non.suara_1": suara_1,
                "validasi_non.suara_2": suara_2,
                "validasi_non.suara_total": suara_total,
                "validasi_non.tps_total": tps_total,
                "validasi_non.tps_masuk": tps_masuk,
                "validasi_non.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kecamatan sukses")

                let kode_wilayah = kode_kecamatan.split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1];
                setTimeout(function () {
                    rekapKota(kode_wilayah, false); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kecamatan error :", error);
            });
        }
    })
    .catch(function(error) {
        console.log("rekap kecamatan error :", error);
    });

}

function rekapKota(kode_kota, status){

    let suara_1 = 0;
    let suara_2 = 0;
    let suara_total = 0;
    let tps_belum = 0;
    let tps_masuk = 0;
    let tps_total = 0;
    db.collection("test_kecamatan")
    .where("kode_kota", "==", kode_kota )
    .get()
    .then(snapshot => {

        if(status === true){
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi.suara_2);
                tps_total = tps_total + Number(doc.data().validasi.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi.tps_masuk); 
            });
        }else{
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi_non.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi_non.suara_2);
                tps_total = tps_total + Number(doc.data().validasi_non.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi_non.tps_masuk); 
            });
        }
        
        suara_total = suara_1+suara_2;
        tps_belum = tps_total-tps_masuk;
        console.log("rekap kota"); 
        console.log("suara_1 : "+suara_1);
        console.log("suara_2 : "+suara_2);
        console.log("suara_total : "+suara_total);
        console.log("tps_total : "+tps_total);
        console.log("tps_masuk : "+tps_masuk);
        console.log("tps_belum : "+tps_belum);

        if(status === true){

            // update rekap kelurahan        
            db.collection("test_kota")
            .doc(kode_kota) 
            .update({
                "validasi.suara_1": suara_1,
                "validasi.suara_2": suara_2,
                "validasi.suara_total": suara_total,
                "validasi.tps_total": tps_total,
                "validasi.tps_masuk": tps_masuk,
                "validasi.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kota sukses")

                let kode_wilayah = kode_kota.split('.');
                kode_wilayah = kode_wilayah[0];
                setTimeout(function () {
                    rekapProvinsi(kode_wilayah, true); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kota error :", error);
            });
        }else{
            
            // update rekap kelurahan        
            db.collection("test_kota")
            .doc(kode_kota) 
            .update({
                "validasi_non.suara_1": suara_1,
                "validasi_non.suara_2": suara_2,
                "validasi_non.suara_total": suara_total,
                "validasi_non.tps_total": tps_total,
                "validasi_non.tps_masuk": tps_masuk,
                "validasi_non.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update kota sukses")

                let kode_wilayah = kode_kota.split('.');
                kode_wilayah = kode_wilayah[0];
                setTimeout(function () {
                    rekapProvinsi(kode_wilayah, false); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update kota error :", error);
            });
        }
    })
    .catch(function(error) {
        console.log("rekap kota error :", error);
    });

}

function rekapProvinsi(kode_provinsi, status){

    let suara_1 = 0;
    let suara_2 = 0;
    let suara_total = 0;
    let tps_belum = 0;
    let tps_masuk = 0;
    let tps_total = 0;
    db.collection("test_kota")
    .where("kode_provinsi", "==", kode_provinsi )
    .get()
    .then(snapshot => {

        if(status === true){
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi.suara_2);
                tps_total = tps_total + Number(doc.data().validasi.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi.tps_masuk); 
            });
        }else{
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi_non.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi_non.suara_2);
                tps_total = tps_total + Number(doc.data().validasi_non.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi_non.tps_masuk); 
            });
        }
        
        suara_total = suara_1+suara_2;
        tps_belum = tps_total-tps_masuk;
        console.log("rekap provinsi"); 
        console.log("suara_1 : "+suara_1);
        console.log("suara_2 : "+suara_2);
        console.log("suara_total : "+suara_total);
        console.log("tps_total : "+tps_total);
        console.log("tps_masuk : "+tps_masuk);
        console.log("tps_belum : "+tps_belum);

        if(status === true){

            // update rekap kelurahan        
            db.collection("test_provinsi")
            .doc(kode_provinsi) 
            .update({
                "validasi.suara_1": suara_1,
                "validasi.suara_2": suara_2,
                "validasi.suara_total": suara_total,
                "validasi.tps_total": tps_total,
                "validasi.tps_masuk": tps_masuk,
                "validasi.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update provinsi sukses")

                // let kode_wilayah = kode_provinsi.split('.');
                // kode_wilayah = kode_wilayah[0];
                setTimeout(function () {
                    rekapNasional(true); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update provinsi error :", error);
            });
        }else{
            
            // update rekap kelurahan        
            db.collection("test_provinsi")
            .doc(kode_provinsi) 
            .update({
                "validasi_non.suara_1": suara_1,
                "validasi_non.suara_2": suara_2,
                "validasi_non.suara_total": suara_total,
                "validasi_non.tps_total": tps_total,
                "validasi_non.tps_masuk": tps_masuk,
                "validasi_non.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update provinsi sukses")

                // let kode_wilayah = kode_provinsi.split('.');
                // kode_wilayah = kode_wilayah[0];
                setTimeout(function () {
                    rekapNasional(false); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("update provinsi error :", error);
            });
        }
    })
    .catch(function(error) {
        console.log("rekap provinsi error :", error);
    });

}

function rekapNasional(status){

    let suara_1 = 0;
    let suara_2 = 0;
    let suara_total = 0;
    let tps_belum = 0;
    let tps_masuk = 0;
    let tps_total = 0;
    db.collection("test_provinsi")
    .get()
    .then(snapshot => {

        if(status === true){
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi.suara_2);
                tps_total = tps_total + Number(doc.data().validasi.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi.tps_masuk); 
            });
        }else{
            snapshot.forEach(doc => { 
                suara_1 = suara_1 + Number(doc.data().validasi_non.suara_1);
                suara_2 = suara_2 + Number(doc.data().validasi_non.suara_2);
                tps_total = tps_total + Number(doc.data().validasi_non.tps_total);
                tps_masuk = tps_masuk + Number(doc.data().validasi_non.tps_masuk); 
            });
        }
        
        suara_total = suara_1+suara_2;
        tps_belum = tps_total-tps_masuk;
        console.log("rekap nasional"); 
        console.log("suara_1 : "+suara_1);
        console.log("suara_2 : "+suara_2);
        console.log("suara_total : "+suara_total);
        console.log("tps_total : "+tps_total);
        console.log("tps_masuk : "+tps_masuk);
        console.log("tps_belum : "+tps_belum);

        if(status === true){

            // update rekap kelurahan        
            db.collection("test_nasional")
            .doc("0") 
            .update({
                "validasi.suara_1": suara_1,
                "validasi.suara_2": suara_2,
                "validasi.suara_total": suara_total,
                "validasi.tps_total": tps_total,
                "validasi.tps_masuk": tps_masuk,
                "validasi.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update nasional sukses")

                // let kode_wilayah = kode_provinsi.split('.');
                // kode_wilayah = kode_wilayah[0];
                // rekapNasional(); 
            })
            .catch(function(error) {
                console.log("update nasional error :", error);
            });
        }else{
            
            // update rekap kelurahan        
            db.collection("test_nasional")
            .doc("0") 
            .update({
                "validasi_non.suara_1": suara_1,
                "validasi_non.suara_2": suara_2,
                "validasi_non.suara_total": suara_total,
                "validasi_non.tps_total": tps_total,
                "validasi_non.tps_masuk": tps_masuk,
                "validasi_non.tps_belum": tps_belum,
            }) 
            .then(function() {
                console.log("update nasional sukses")

                // let kode_wilayah = kode_provinsi.split('.');
                // kode_wilayah = kode_wilayah[0];
                // rekapNasional(); 
            })
            .catch(function(error) {
                console.log("update nasional error :", error);
            });
        }
    })
    .catch(function(error) {
        console.log("rekap nasional error :", error);
    });

}

exports.countCollection = functions.https.onRequest((request, response) => {
    cors(request, response, () => {


    let provinsi = 0;
    let kota = 0;
    let kecamatan = 0;
    let kelurahan = 0;
    let tps = 0;
    let users = 0;

    db.collection('ref_provinsi').get().then(snap_provinsi => {
        console.log("provinsi " + snap_provinsi.size);
        provinsi = snap_provinsi.size;    
        
        db.collection('ref_kota').get().then(snap_kota => {
            console.log("kota " + snap_kota.size);
            kota = snap_kota.size;    

            db.collection('ref_kecamatan').get().then(snap_kecamatan => {
                console.log("kecamatan " + snap_kecamatan.size);
                kecamatan = snap_kecamatan.size;    
 
                db.collection('ref_kelurahan').get().then(snap_kelurahan => {
                    console.log("kelurahan " + snap_kelurahan.size);
                    kelurahan = snap_kelurahan.size;    
                    
                    db.collection('ref_tps').get().then(snap_tps => {
                        console.log("tps " + snap_tps.size);
                        tps = snap_tps.size;    
                        
                        db.collection('users').get().then(snap_users => {
                            console.log("users " + snap_users.size);
                            users = snap_users.size;    
 
                            response.send({
                                provinsi: provinsi,
                                kota: kota,
                                kecamatan: kecamatan,
                                kelurahan: kelurahan,
                                tps: tps,
                                users: users
                            });
                        });
                    });
                });
            });
        });
    });    

    });

});

exports.insertSuaraTPS = functions.https.onRequest(async(request, response) => {
    cors(request, response, async() => {

    const res = await checkToken(request);
    const data_status = res.status;
    const data_message = res.message;
    const data_result = res.result;

    if(data_status === false){
        response.status(400).send({
            status: data_status,
            message: data_message,
            result: data_result
        });
    }
    else{
    
        if(!request.body['kode_tps'] || !request.body['suara_total'] || !request.body['suara_1'] || !request.body['suara_2'] || !request.body['suara_scan_1'] || !request.body['suara_scan_2'] || !request.body['suara_scan_3'] || !request.body['suara_latitude'] || !request.body['suara_longitude']){
    
            response.status(400).send({
                ok: false,
                result: null,
                message: "Bad argument"
            });
    
        } else {


            const data_json = {};
            let data_user = {};
 
            data_json["kode_tps"] = request.body['kode_tps']; 
            data_json["suara_total"] = request.body['suara_total'];
            data_json["suara_sah"] = request.body['suara_sah'];
            data_json["suara_tidaksah"] = request.body['suara_tidaksah'];
            data_json["suara_1"] = request.body['suara_1'];
            data_json["suara_2"] = request.body['suara_2'];
            data_json["suara_scan_1"] = request.body['suara_scan_1'];
            data_json["suara_scan_2"] = request.body['suara_scan_2'];
            data_json["suara_scan_3"] = request.body['suara_scan_3'];
            data_json["suara_latitude"] = request.body['suara_latitude'];
            data_json["suara_longitude"] = request.body['suara_longitude']; 
            data_json["suara_date"] = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");

            // update tps
            db.collection("test_tps")
            .doc(request.body['kode_tps'])
            .update(data_json)
            .then(function() {
                console.log("Tps updated");   
                let kode_wilayah = request.body['kode_tps'].split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1]+"."+kode_wilayah[2]+"."+kode_wilayah[3]
                setTimeout(function () {
                    rekapKelurahan(kode_wilayah, false); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("Error update tps:", error);
            });


            setTimeout(function () {
                // get last tps
                db.collection('test_tps')
                .doc(request.body['kode_tps'])
                .get()
                .then(function(doc) {
                    console.log("Tps retrieved");
                    data_user = doc.data();
                    response.send({
                        ok: true,
                        result: data_user,
                        message: "Suara TPS Inserted"
                    });
                })
                .catch(function(error) {
                    console.log("Error getting tps:", error);
                });
            }, 2000);
             

            
        }
               
    }     
    });
});

exports.verifikasiSuaraTPS = functions.https.onRequest(async(request, response) => {
    cors(request, response, async() => {

    // const res = await checkToken(request);
    // const data_status = res.status;
    // const data_message = res.message;
    // const data_result = res.result;

    // if(data_status === false){
    //     response.status(400).send({
    //         status: data_status,
    //         message: data_message,
    //         result: data_result
    //     });
    // }
    // else{
    
        if(!request.body['kode_tps'] || !request.body['suara_total'] || !request.body['suara_1'] || !request.body['suara_2']){
    
            response.status(400).send({
                ok: false,
                result: null,
                message: "Bad argument"
            });
    
        } else {


            const data_json = {};
            let data_user = {};
 
            data_json["kode_tps"] = request.body['kode_tps']; 
            data_json["suara_total"] = request.body['suara_total'];
            data_json["suara_1"] = request.body['suara_1'];
            data_json["suara_2"] = request.body['suara_2'];
            data_json["status_validasi_date"] = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
            data_json["status_validasi"] = true;

            // update tps
            db.collection("test_tps")
            .doc(request.body['kode_tps'])
            .update(data_json)
            .then(function() {
                console.log("Tps updated");   
                let kode_wilayah = request.body['kode_tps'].split('.');
                kode_wilayah = kode_wilayah[0]+"."+kode_wilayah[1]+"."+kode_wilayah[2]+"."+kode_wilayah[3]
                setTimeout(function () {
                    rekapKelurahan(kode_wilayah, true); 
                }, 2000);
            })
            .catch(function(error) {
                console.log("Error update tps:", error);
            });


            setTimeout(function () {
                // get last tps
                db.collection('test_tps')
                .doc(request.body['kode_tps'])
                .get()
                .then(function(doc) {
                    console.log("Tps retrieved");
                    data_user = doc.data();
                    response.send({
                        ok: true,
                        result: data_user,
                        message: "Suara TPS Inserted"
                    });
                })
                .catch(function(error) {
                    console.log("Error getting tps:", error);
                });
            }, 2000);
             

            
        }
               
    // }     
    });
});

exports.updateUser = functions.https.onRequest(async(request, response) => {
    cors(request, response, async() => {

    const res = await checkToken(request);
    const data_status = res.status;
    const data_message = res.message;
    const data_result = res.result;

    if(data_status === false){
        response.status(400).send({
            status: data_status,
            message: data_message,
            result: data_result
        });
    }
    else{
    
        if(!request.body['id']){
    
            response.status(400).send({
                ok: false,
                result: null,
                message: "Bad argument"
            });
    
        } else {


            
            const data_json = {};
            let data_user = {};

            if(typeof request.body['alamat'] !== "undefined"){ 
                data_json["alamat"] = request.body['alamat'];
            }
            if(typeof request.body['nama'] !== "undefined"){ 
                data_json["nama"] = request.body['nama'];
            }
            if(typeof request.body['username'] !== "undefined"){ 
                data_json["username"] = request.body['username'];
            }
            if(typeof request.body['password'] !== "undefined"){ 
                data_json["password"] = md5(request.body['password']);
            }
            if(typeof request.body['tanggal_lahir'] !== "undefined"){ 
                data_json["tanggal_lahir"] = request.body['tanggal_lahir'];
            }
            if(typeof request.body['tempat_lahir'] !== "undefined"){ 
                data_json["tempat_lahir"] = request.body['tempat_lahir'];
            }
            if(typeof request.body['no_hp'] !== "undefined"){ 
                data_json["no_hp"] = request.body['no_hp'];
            }
            if(typeof request.body['status_absen'] !== "undefined"){ 
                data_json["status_absen"] = request.body['status_absen'];
                
                if(data_json["status_absen"] === true){
                    if(typeof request.body['latitude'] !== "undefined"){ 
                        data_json["latitude"] = request.body['latitude'];
                    }
                    if(typeof request.body['longitude'] !== "undefined"){ 
                        data_json["longitude"] = request.body['longitude'];
                    } 
                }
            } 
            if(typeof request.body['status_kirim'] !== "undefined"){ 
                data_json["status_kirim"] = request.body['status_kirim'];
            } 
            data_json["last_access"] = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");

            // update user
            db.collection("test_user")
            .doc(request.body['id'])
            .update(data_json)
            .then(function() {
                console.log("Users updated"); 
            })
            .catch(function(error) {
                console.log("Error update user:", error);
            });

            setTimeout(function () {
                // get last data
                db.collection('test_user')
                .doc(request.body['id'])
                .get()
                .then(function(doc) {
                    console.log("Users retrieved");
                    data_user = doc.data();
                    response.send({
                        ok: true,
                        result: data_user,
                        message: "Users updated"
                    });
                })
                .catch(function(error) {
                    console.log("Error getting user:", error);
                });
            }, 2000);
             
            
             

            
        }
               
    }     
    });
});

exports.login = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
 
    if(!request.body['kode_tps'] || !request.body['username'] || !request.body['password']){

        response.status(400).send({
            ok: false,
            result: null,
            message: "Bad argument"
        });

    } else {

        const data_response = {};
        db.collection("test_user")
        .where("kode_tps", "==", request.body['kode_tps'])
        .where("username", "==", request.body['username'])
        .where("password", "==", md5(request.body['password']))
        .get()
        .then(snapshot => {

            if(snapshot.size > 0) {

                snapshot.forEach(doc => {

                    updateLastAccess(request.body['kode_tps'], dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"));

                    const payload = {
                        "_id": doc.data()._id,
                        "id": doc.id,
                        "iat": (new Date().getTime()) / 1000,
                        "exp": (new Date().getTime() + 24 * 60 * 60 * 1000) / 1000
                    };
    
                    const data = {
                        "_id": doc.data()._id,
                        "id": doc.id,
                        "kode_provinsi": doc.data().kode_provinsi,
                        "kode_kota": doc.data().kode_kota,
                        "kode_kecamatan": doc.data().kode_kecamatan,
                        "kode_kelurahan": doc.data().kode_kelurahan,
                        "kode_tps": doc.data().kode_tps,
                        "nama_provinsi": doc.data().nama_provinsi,
                        "nama_kota": doc.data().nama_kota,
                        "nama_kecamatan": doc.data().nama_kecamatan,
                        "nama_kelurahan": doc.data().nama_kelurahan,
                        "nama_tps": doc.data().nama_tps,
                        "username": doc.data().username,
                        "nama": doc.data().nama,
                        "alamat": doc.data().alamat,
                        "no_hp": doc.data().no_hp,
                        "tempat_lahir": doc.data().tempat_lahir,
                        "tanggal_lahir": doc.data().tanggal_lahir,
                        "last_access": doc.data().last_access,
                        "latitude": doc.data().latitude,
                        "longitude": doc.data().longitude,
                        "jarak": doc.data().jarak,
                        "status_absen": doc.data().status_absen,
                        "status_kirim": doc.data().status_kirim,
                        "jarak_status": doc.data().jarak_status,
                        "iat": (new Date().getTime()) / 1000,
                        "exp": (new Date().getTime() + 24 * 60 * 60 * 1000) / 1000
                    };
        
                    const secret = 'secret'; 
                    const token = jwt.encode(payload, secret, 'HS256');
    
                    data_response['users'] = data;
                    data_response['jwt'] = token;
                
                    response.send({
                        status: true,
                        message: "Login Successful",
                        result: data_response
                    });
 
                });

            } else {

                response.send({
                    status: false,
                    message: "Login Failed, invalid username or password",
                    result: {}
                });

            }
             
        }).catch(reason => {
            response.status(400).send({
                status: false,
                message: reason,
                result: {}
            });
        })
    }
 
    });
});


exports.trackingSaksi = functions.https.onRequest((request, response) => {
    
    let result = {};
    let features = [];
    let arr = {};
    // get last tps
    db.collection('test_user')
    .get()
    .then(snapshot => {

        let i = 0;
        snapshot.forEach(doc => {
            console.log("saksi retrieved");
            result = doc.data();
            i = i + 1;

            arr = {
                "geometry": {
                    "coordinates": [parseFloat(doc.data().longitude), parseFloat(doc.data().latitude)],
                    "type": "Point"
                },
                "id": i.toString(),
                "type": "Feature",
                "properties": {
                    "id": i,
                    "kode_tps": doc.data().kode_tps,
                    "nama_provinsi": doc.data().nama_provinsi,
                    "nama_kota": doc.data().nama_kota,
                    "nama_kecamatan": doc.data().nama_kecamatan,
                    "nama_kelurahan": doc.data().nama_kelurahan,
                    "nama_tps": doc.data().nama_tps,
                    "nama": doc.data().nama,
                    "no_hp": doc.data().no_hp,
                    "last_access": doc.data().last_access,
                }
            }
            features.push(arr);
            
        });

        result = {
            "type": "FeatureCollection",
            "metadata": {
                "generated": 1459785466000,
                "url": "http://localhost:4200",
                "title": "Saksi",
                "status": 200,
                "api": "1.5.0",
                "count": 35
            },
            "features": features
        };

        response.send(result);
    })
    .catch(function(error) {
        console.log("Error getting tps:", error);
    });
});